package uz.pdp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import uz.pdp.controller.AuthController;
import uz.pdp.controller.CabinetController;
import uz.pdp.utils.AppConstants;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.csrf().disable();

        httpSecurity.authorizeHttpRequests()
                .requestMatchers(AuthController.BASE_PATH + "/**").permitAll()
                .requestMatchers(AppConstants.APP_BASE_PATH + "/**").authenticated()
                .anyRequest().permitAll();

        httpSecurity.formLogin()
                .usernameParameter("username")
                .loginPage(AuthController.BASE_PATH + AuthController.LOGIN_PATH)
                .loginProcessingUrl(AuthController.BASE_PATH + AuthController.LOGIN_PATH)
                .defaultSuccessUrl(CabinetController.BASE_PATH, true);
        //   httpSecurity.rememberMe()
//                .alwaysRemember(true);
//                .permitAll();

//        httpSecurity
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
//                .invalidSessionUrl(AuthController.BASE_PATH + AuthController.LOGIN_PATH);
        return httpSecurity.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        //userdetailsservice
        //passwordencoder
        return authenticationConfiguration.getAuthenticationManager();
    }
}
