package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.utils.AppConstants;
@RequestMapping(CabinetController.BASE_PATH)
@Controller
public interface CabinetController {
    String BASE_PATH = AppConstants.BASE_PATH + "/cabinet";

    @GetMapping
    String cabinet();
}
