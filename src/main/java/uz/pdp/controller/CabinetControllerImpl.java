package uz.pdp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor

public class CabinetControllerImpl implements CabinetController{
    @Override
    public String cabinet() {
        return "/cabinet/home";
    }
}
