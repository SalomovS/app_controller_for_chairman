package uz.pdp.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.dto.SimCardDTO;
import uz.pdp.model.Tariff;
import uz.pdp.utils.AppConstants;

@Controller
@RequestMapping(TariffController.BASE_PATH)

public interface TariffController {

    String BASE_PATH = AppConstants.BASE_PATH + "/tariff";
    String DELETE_TARIFF = "/delete/{id}";
    String TARIFF_PAGE = "/tariffPage";
    String EDIT_TARIFF = "/edit/{id}";

    @GetMapping(TARIFF_PAGE)
    String list(Model model);

    @PostMapping(TARIFF_PAGE)
    String add(@Valid @ModelAttribute Tariff tariff);

    @GetMapping(DELETE_TARIFF)
    String deleteTariff(@PathVariable Integer id);

    @PostMapping(EDIT_TARIFF)
    String editTariff(@PathVariable Integer id,@ModelAttribute Tariff tariff);


}
