package uz.pdp.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.dto.SimCardDTO;
import uz.pdp.utils.AppConstants;
@Controller
@RequestMapping(NumberController.BASE_PATH)

public interface NumberController {

    String BASE_PATH = AppConstants.BASE_PATH + "/sim";
    String NUMBER_PATH = "/simCard";
    String DELETE_NUMBER = "/delete";

    @GetMapping(NUMBER_PATH)
    String list(Model model);

    @PostMapping(NUMBER_PATH)
    String add(@Valid @ModelAttribute SimCardDTO simCardDTO);

    @GetMapping(DELETE_NUMBER+"/{id}")
    String deleteNumber(@PathVariable("id") Integer id);


}
