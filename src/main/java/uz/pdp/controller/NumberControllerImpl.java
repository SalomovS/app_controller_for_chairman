package uz.pdp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseBody;
import uz.pdp.dto.SimCardDTO;
import uz.pdp.model.SimCard;
import uz.pdp.service.SimCardService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class NumberControllerImpl implements NumberController{
    private final SimCardService simCardService;
    @Override
    public String list(Model model) {
        List<SimCard> list = simCardService.list();
        model.addAttribute("simlist",list);
        return "/sim/simCard";
    }

    @Override
    public String add(SimCardDTO simCardDTO) {
        boolean add = simCardService.add(simCardDTO);
        if (!add) {
            return "redirect:"+BASE_PATH+NUMBER_PATH ;

        }
        return "redirect:"+BASE_PATH+NUMBER_PATH ;
    }

    @Override
    public String deleteNumber(Integer id) {
        boolean delete = simCardService.delete(id);

        if (delete) {
            return "redirect:"+BASE_PATH+NUMBER_PATH;
        }
        return "redirect:"+BASE_PATH+NUMBER_PATH;
    }
}
