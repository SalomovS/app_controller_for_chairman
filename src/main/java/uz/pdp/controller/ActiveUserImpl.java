package uz.pdp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import uz.pdp.model.ActiveUser;
import uz.pdp.service.SimCardService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ActiveUserImpl implements ActiveUsers{
    private final SimCardService simCardService;

    @Override
    public String showUser(Model model) {
        List<ActiveUser> users=simCardService.activeUser();
        model.addAttribute("userList",users);
        return "/history/historyPage";

    }

    @Override
    public String blockUser(Integer id) {

        simCardService.blockUser(id);
        return "redirect:"+BASE_PATH+SHOW_PATH;
    }

    @Override
    public String openBlockUser(Integer id) {

        simCardService.openBlockUser(id);
        return "redirect:"+BASE_PATH+SHOW_PATH;
    }
}
