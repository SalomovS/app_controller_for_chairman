package uz.pdp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import uz.pdp.dto.LoginDTO;
import uz.pdp.service.AuthService;

@Controller
@RequiredArgsConstructor

public class AuthControllerImpl implements AuthController {

    private final AuthService authService;
    @Override
    public String loginPage() {
        return "/auth/login";
    }

    @Override
    public String logoutPage() {
        return "/auth/logout1";
    }

    @Override
    public String login(LoginDTO loginDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            throw new RuntimeException();

        boolean ok = authService.signIn(loginDTO);
        if (!ok)
            return "redirect:" + AuthController.BASE_PATH + AuthController.LOGIN_PATH;

        return "redirect:/api/cabinet";

    }
}
