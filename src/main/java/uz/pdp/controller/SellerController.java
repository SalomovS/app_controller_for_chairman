package uz.pdp.controller;


import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.pdp.dto.GiveNumberDTO;
import uz.pdp.model.Customer;
import uz.pdp.utils.AppConstants;

@RequestMapping(SellerController.BASE_PATH)
@Controller
public interface SellerController {
    String BASE_PATH = AppConstants.BASE_PATH + "/seller";
    String SELLER_PATH = "/sellerPage";
    String SALE_PATH = "/sale";
    String TARIFF_PATH = "/tariff";
    String ALL_NUMBER_PATH = "/numbers";
    String CUSTOMER_INFO = "/customer";
    String CHOOSE_PATH = "/chooseN";

    @GetMapping(SELLER_PATH)
    String sellerPage();

    @GetMapping(SALE_PATH)
    String salePage();

    @GetMapping(TARIFF_PATH)
    String tariffPage(Model model);

    @GetMapping(ALL_NUMBER_PATH)
    String numbersPage(Model model);

    @GetMapping(CHOOSE_PATH)
    String choosePage(Model model);
    @PostMapping
    String giveNumPage( @RequestParam String customerId, @RequestParam Integer simcardNumber, @RequestParam Integer tariff );

    @PostMapping(CUSTOMER_INFO)
    String addInfoCustomer(@Valid @ModelAttribute Customer customer,Model model);


}
