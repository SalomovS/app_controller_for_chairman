package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.utils.AppConstants;

@Controller
@RequestMapping(ActiveUsers.BASE_PATH)
public interface ActiveUsers {
    String BASE_PATH = AppConstants.BASE_PATH + "/history";
    String SHOW_PATH = "/historyPage";
    String BLOCK_PATH = "/{id}";
    String OPEN_BLOCK_PATH = "/open/{id}";

    @GetMapping(SHOW_PATH)
    String showUser(Model model);

    @GetMapping(BLOCK_PATH)
    String blockUser(@PathVariable Integer id);

    @GetMapping(OPEN_BLOCK_PATH)
    String openBlockUser(@PathVariable Integer id);
}
