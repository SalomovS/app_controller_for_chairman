package uz.pdp.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.controller.TariffController;
import uz.pdp.model.Tariff;
import uz.pdp.service.TariffService;
import uz.pdp.utils.AppConstants;

@Controller
public class TariffControllerImpl implements TariffController {
    private final TariffService tariffService;

    @Autowired
    public TariffControllerImpl(TariffService tariffService) {
        this.tariffService = tariffService;
    }

    @Override
    public String list(Model model) {
        model.addAttribute("tariffs", tariffService.list());
        return "tariff/tariffPage";
    }

    @Override
    public String add( Tariff tariff) {
        Tariff newTariff = new Tariff();
        newTariff.setName(tariff.getName());
        newTariff.setDescription(tariff.getDescription());
        tariffService.add(newTariff);
        return "redirect:" + BASE_PATH+TARIFF_PAGE;
    }
    @Override
    public String deleteTariff( Integer id) {
        tariffService.delete(id);
        return "redirect:" + BASE_PATH+TARIFF_PAGE;
    }

    @Override
    public String editTariff(Integer id,Tariff tariff) {

        tariffService.update(id,tariff);

        return "redirect:" + BASE_PATH;
    }
}
