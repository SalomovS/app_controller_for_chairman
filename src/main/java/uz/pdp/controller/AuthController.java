package uz.pdp.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.dto.LoginDTO;
import uz.pdp.utils.AppConstants;

@RequestMapping(AuthController.BASE_PATH)
@Controller
public interface AuthController {
    String BASE_PATH = AppConstants.BASE_PATH + "/auth";
    String LOGIN_PATH = "/login";
    String LOGOUT_PATH = "/logout1";

    @GetMapping(LOGIN_PATH)
    String loginPage();

    @GetMapping(LOGOUT_PATH)
    String logoutPage();

    @PostMapping(LOGIN_PATH)
    String login(@Valid @ModelAttribute LoginDTO loginDTO, BindingResult bindingResult);

}
