package uz.pdp.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.dto.GiveNumberDTO;
import uz.pdp.model.Customer;
import uz.pdp.service.SellerService;
import uz.pdp.service.SimCardService;
import uz.pdp.service.TariffService;
import uz.pdp.utils.AppConstants;

@Controller
@RequiredArgsConstructor
public class SellerControllerImpl implements SellerController {
    private final SimCardService simCardService;
    private final TariffService tariffService;
    private final SellerService sellerService;

    @Override
    public String sellerPage() {
        return "/seller/sellerPage";
    }

    @Override
    public String salePage() {
        return "/seller/sale";
    }

    @Override
    public String tariffPage(Model model) {
        model.addAttribute("tariffs", tariffService.list());

        return "/seller/tariff";
    }

    @Override
    public String numbersPage(Model model) {
        model.addAttribute("simList", simCardService.list());
        return "/seller/numbers";
    }

    @Override
    public String choosePage(Model model) {
        return "redirect:" + BASE_PATH + "/chooseN";
    }

    @Override
    public String giveNumPage(String customerId, Integer simcardNumber, Integer tariff) {

        sellerService.giveNumberToUser(customerId,simcardNumber,tariff);
        sellerService.insertActiveUser(customerId,simcardNumber,tariff);
        return "/seller/sale";
    }


    @Override
    public String addInfoCustomer(Customer customer, Model model) {
        Customer build = Customer.builder().fullName(customer.getFullName())
                .address(customer.getAddress())
                .passportID(customer.getPassportID()).build();
        sellerService.add(build);
        model.addAttribute("customer", customer);
        model.addAttribute("numbers",simCardService.list());
        model.addAttribute("tariffs",tariffService.list());
        return "/seller/chooseN";
    }
}
