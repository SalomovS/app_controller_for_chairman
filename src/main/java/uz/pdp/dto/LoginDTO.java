package uz.pdp.dto;

public record LoginDTO(String username, String password) {
}
