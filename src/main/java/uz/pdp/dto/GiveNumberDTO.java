package uz.pdp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GiveNumberDTO {
    private String customerId;
    private Integer simcardNumber;
    private Integer tariff;

}
