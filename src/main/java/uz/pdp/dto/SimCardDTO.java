package uz.pdp.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

@Data
public class SimCardDTO {

    @NotBlank
    @Size(max = 9,min = 9)
    private String number;

}
