package uz.pdp.service;

import org.springframework.stereotype.Service;
import uz.pdp.dao.SimCardDAO;
import uz.pdp.dto.SimCardDTO;
import uz.pdp.model.ActiveUser;
import uz.pdp.model.SimCard;

import java.util.List;
@Service

public class SimCardServiceImpl implements SimCardService{

    private final SimCardDAO simCardDAO;

    public SimCardServiceImpl(SimCardDAO simCardDAO) {
        this.simCardDAO = simCardDAO;
    }

    @Override
    public List<SimCard> list() {
        List<SimCard> list =simCardDAO.list();
        return list;
    }

    @Override
    public boolean add(SimCardDTO simCardDTO) {
        return simCardDAO.add(simCardDTO);
    }

    @Override
    public boolean delete(int id) {
        return simCardDAO.delete(id);

    }

    @Override
    public List<ActiveUser> activeUser() {

        return  simCardDAO.getAllActiveUser();

    }

    @Override
    public void blockUser(Integer id) {
        simCardDAO.blockUser(id);
    }

    @Override
    public void openBlockUser(Integer id) {
        simCardDAO.openBlock(id);
    }
}
