package uz.pdp.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import uz.pdp.dto.LoginDTO;

public interface AuthService extends UserDetailsService {

    boolean signIn(LoginDTO loginDTO);
}
