package uz.pdp.service;

import org.springframework.stereotype.Service;
import uz.pdp.dto.SimCardDTO;
import uz.pdp.model.ActiveUser;
import uz.pdp.model.SimCard;

import java.util.List;

@Service

public interface SimCardService {

    List<SimCard> list ();

    boolean add(SimCardDTO simCardDTO);

    boolean delete(int id);

    List<ActiveUser> activeUser();

    void blockUser(Integer id);

    void openBlockUser(Integer id);
}
