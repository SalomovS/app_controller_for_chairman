package uz.pdp.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.dao.UserDAO;
import uz.pdp.dto.LoginDTO;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserDAO userDAO;
    private final AuthenticationManager authenticationManager;

    public AuthServiceImpl(UserDAO userDAO, @Lazy AuthenticationManager authenticationManager) {
        this.userDAO = userDAO;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public boolean signIn(LoginDTO loginDTO) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(loginDTO.username(), loginDTO.password());
        Authentication authentication;
        try {
            authentication =
                    authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        } catch (AuthenticationException e) {
            throw new RuntimeException(e);
        }

        //load user
        //user password match
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDAO.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("user nor fot dound"));
    }

}
