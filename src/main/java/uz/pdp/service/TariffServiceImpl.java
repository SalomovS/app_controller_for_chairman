package uz.pdp.service;

import org.springframework.stereotype.Service;
import uz.pdp.dao.TariffDAO;
import uz.pdp.model.Tariff;

import java.util.List;

@Service
public class TariffServiceImpl implements TariffService{
   private final TariffDAO tariffDAO;

    public TariffServiceImpl(TariffDAO tariffDAO) {
        this.tariffDAO = tariffDAO;
    }

    @Override
    public List<Tariff> list() {
        return tariffDAO.list();

    }

    @Override
    public boolean add(Tariff tariff) {
       return tariffDAO.add(tariff);
    }

    @Override
    public boolean edit(int id) {
        return false;
    }

    @Override
    public boolean delete(int id) {
       return tariffDAO.delete(id);
    }

    @Override
    public Tariff getById(Integer id) {
        return tariffDAO.getById(id);

    }

    @Override
    public boolean update(Integer id, Tariff newTariff) {

        Tariff oldTariff = tariffDAO.getById(id);

        return tariffDAO.update(id, newTariff);
    }
}
