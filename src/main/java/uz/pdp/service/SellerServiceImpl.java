package uz.pdp.service;

import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import uz.pdp.dao.SellerDAO;
import uz.pdp.dto.GiveNumberDTO;
import uz.pdp.model.Customer;

import java.util.Optional;

@Service
public class SellerServiceImpl implements SellerService {
    private final SellerDAO sellerDAO;

    public SellerServiceImpl(SellerDAO sellerDAO) {
        this.sellerDAO = sellerDAO;
    }

    @Override
    public boolean add(Customer build) {

        Optional<Customer> byPassport = sellerDAO.getByPassport(build.getPassportID());
        if (byPassport.isPresent()) {
            return true;
        }

        return sellerDAO.add(build);
    }

    @Override
    public boolean giveNumberToUser(String customerId, Integer simcardNumber, Integer tariff) {
        boolean b = sellerDAO.changeActive(simcardNumber);
        boolean b1 = sellerDAO.giveNumberToUser(customerId,simcardNumber,tariff);
        return b1 && b;


    }

    @Override
    public boolean insertActiveUser(String customerId, Integer simcardNumber, Integer tariff) {
       String fullName= sellerDAO.getFullNameByPassport(customerId);
       Long number = sellerDAO.getNumberBySimcardId(simcardNumber);
       String tariffName = sellerDAO.getNameByTArifID(tariff);
        return sellerDAO.insertToActiveTable(fullName, number, tariffName);
    }


}
