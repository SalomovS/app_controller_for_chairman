package uz.pdp.service;

import org.springframework.stereotype.Component;
import uz.pdp.model.Tariff;

import java.util.List;

@Component
public interface TariffService {

    List<Tariff> list();

    boolean add(Tariff tariff);

    boolean edit( int id);

    boolean delete( int id);


    Tariff getById(Integer id);

    boolean update(Integer id, Tariff existingTariff);
}
