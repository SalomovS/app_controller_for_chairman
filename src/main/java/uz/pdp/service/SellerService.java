package uz.pdp.service;

import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import uz.pdp.dto.GiveNumberDTO;
import uz.pdp.model.Customer;

@Service
public interface SellerService {
    boolean add(Customer build);


    boolean giveNumberToUser(String customerId, Integer simcardNumber, Integer tariff);

    boolean insertActiveUser(String customerId, Integer simcardNumber, Integer tariff);
}
