package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ActiveUser {
    private Integer id;
    private String fullName;
    private Long numbers;
    private String tariff;
    private boolean blocked;

}
