package uz.pdp.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Tariff {
    private Integer id;
    private String name;
    private String description;
}
