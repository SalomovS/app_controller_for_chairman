package uz.pdp.model;

import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Customer {
    private Integer id;
    @NotBlank
    private String passportID;
    private String fullName;
    private String address;

}
