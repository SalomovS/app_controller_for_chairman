package uz.pdp.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SimCard {

    private Integer id;
    private String number;
    private boolean active;
    private Tariff tariff;



}
