package uz.pdp.utils;

public interface AppConstants {
    String APP_BASE_PATH = "/api";
    String APP_VERSION_V1 = "/v1";

    String BASE_PATH = APP_BASE_PATH + APP_VERSION_V1;

    //    https://ketmon.uz/api/v1/auth
}
