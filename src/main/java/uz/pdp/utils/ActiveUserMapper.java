package uz.pdp.utils;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import uz.pdp.model.ActiveUser;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActiveUserMapper implements RowMapper<ActiveUser> {
    @Override
    public ActiveUser mapRow(ResultSet rs, int rowNum) throws SQLException {

       return ActiveUser.builder().fullName(rs.getString("fullname"))
               .id(rs.getInt("id"))
                .numbers(rs.getLong("number"))
                .tariff(rs.getString("tariff"))
                .blocked(rs.getBoolean("blocked")).build();
    }
}
