package uz.pdp.utils;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import uz.pdp.model.Tariff;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TariffMapper implements RowMapper<Tariff> {
    @Override
    public Tariff mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Tariff.builder().id(rs.getInt("id"))
                .name(rs.getString("name"))
                .description(rs.getString("description")).build();

    }
}
