package uz.pdp.utils;

import uz.pdp.model.Role;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommonUtils {

    public static Role mapRole(ResultSet rs, String pr) {
        try {
            return Role.builder()
                    .id(rs.getInt( pr+"id"))
                    .name(rs.getString( pr+"name"))
                    .description(rs.getString( pr+"description"))
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
