package uz.pdp.payload;

import org.springframework.jdbc.core.RowMapper;
import uz.pdp.model.Role;
import uz.pdp.model.User;
import uz.pdp.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        Role role = CommonUtils.mapRole(rs,"r_");
        return User.builder().username(rs.getString("u_username")).role(role).password(rs.getString("u_password")).enabled(rs.getBoolean("u_enabled")).build();

    }
}
