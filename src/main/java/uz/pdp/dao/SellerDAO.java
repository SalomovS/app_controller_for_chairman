package uz.pdp.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import uz.pdp.model.Customer;
import uz.pdp.model.SimCard;
import uz.pdp.model.Tariff;
import uz.pdp.utils.CustomerMapper;

import java.util.Optional;

@Service
public class SellerDAO {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public SellerDAO(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean add(Customer build) {

        String sql = "insert into customer (full_name, passport_number,address) values  ( :i_name, :i_passport, :i_address) ";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_name", build.getFullName())
                .addValue("i_passport", build.getPassportID())
                .addValue("i_address", build.getAddress());
        int update = jdbcTemplate.update(sql, parameterSource);
        return update > 0;
    }

    public Optional<Customer> getByPassport(String passportID) {
       Customer customer;
        try {
            String sql = "select * from customer where passport_number = :i_passportId";
            MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_passportId", passportID);
             customer = jdbcTemplate.queryForObject(sql, parameterSource, new CustomerMapper());
        } catch (Exception e) {
            return Optional.empty();
        }

        return Optional.ofNullable(customer);
    }

    public boolean giveNumberToUser(String customerId, Integer simcardNumber, Integer tariff) {
        String sql = "insert into customer_simcard(customer_passport,simcard_id,tariff_id) values (:i_cId, :i_sId, :i_tId)";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_cId",customerId)
                .addValue("i_sId",simcardNumber)
                .addValue("i_tId",tariff);
        int update = jdbcTemplate.update(sql, parameterSource);
        return update>0;

    }

    public boolean changeActive(Integer simcardNumber) {

        String sql = "Update  sim_card set active = true where id = :i_id ";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", simcardNumber);
        int update = jdbcTemplate.update(sql, parameterSource);
        return update>0;
    }

    public String getFullNameByPassport(String customerId) {
        String sql = "Select full_name from customer where passport_number = :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", customerId);
        Customer customer = jdbcTemplate.queryForObject(sql, parameterSource, BeanPropertyRowMapper.newInstance(Customer.class));

        return customer.getFullName();
    }

    public Long getNumberBySimcardId(Integer id) {

        String sql = "Select number from sim_card where id = :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id",id );
        SimCard simCard = jdbcTemplate.queryForObject(sql, parameterSource, BeanPropertyRowMapper.newInstance(SimCard.class));

        return Long.valueOf(simCard.getNumber());
    }

    public String getNameByTArifID(Integer id) {

        String sql = "Select name from tariff where id = :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id",id );
        Tariff simCard = jdbcTemplate.queryForObject(sql, parameterSource, BeanPropertyRowMapper.newInstance(Tariff.class));

        return simCard.getName();
    }

    public boolean insertToActiveTable(String fullName, Long number, String tariffName) {

        String sql = "insert into activeusers (fullname,number,tariff) values (:f, :n, :t)";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("f", fullName).addValue("n", number).addValue("t", tariffName);
        int update = jdbcTemplate.update(sql, parameterSource);
        return update>0;
    }
}
