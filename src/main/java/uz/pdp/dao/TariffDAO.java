package uz.pdp.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import uz.pdp.model.Tariff;
import uz.pdp.utils.TariffMapper;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TariffDAO {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public List<Tariff> list() {
        String sql = "select * from tariff";
        return jdbcTemplate.query(sql, new TariffMapper());
    }

    public boolean add(Tariff tariff) {
        String sql = "insert into tariff(name,description) values (:i_name,:i_desc)";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_name", tariff.getName()).addValue("i_desc", tariff.getDescription());
        int update = jdbcTemplate.update(sql, parameterSource);
        return update > 0;

    }

    public boolean delete(int id) {

        String sql = "delete from tariff where id= :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", id);
        int update = jdbcTemplate.update(sql, parameterSource);
        return update > 0;
    }

    public Tariff getById(Integer id) {

        String sql = "select * from tariff where id= :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", id);
        return jdbcTemplate.queryForObject(sql, parameterSource, new TariffMapper());

    }

    public boolean update(Integer id, Tariff newTariff) {
        String sql = "Update tariff set name = :i_name, description = :i_desc  where id = :i_id ";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_name", newTariff.getName())
                .addValue("i_desc", newTariff.getDescription())
                .addValue("i_id", id);
        int update = jdbcTemplate.update(sql, parameterSource);
        return update > 0;


    }
}
