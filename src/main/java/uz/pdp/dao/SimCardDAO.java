package uz.pdp.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import uz.pdp.dto.SimCardDTO;
import uz.pdp.model.ActiveUser;
import uz.pdp.model.SimCard;
import uz.pdp.utils.ActiveUserMapper;

import java.sql.ResultSet;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service
public class SimCardDAO {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public SimCardDAO(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<SimCard> list() {

        String sql = "select * from sim_card where active=false";
        List<SimCard> number = jdbcTemplate.query(sql, new RowMapper() {
            @Override
            public SimCard mapRow(ResultSet rs, int rowNum) throws SQLException {

                return SimCard.builder().id(rs.getInt("id")).number(String.valueOf(rs.getLong("number"))).build();

            }
        });
        return number;


    }

    public boolean delete(Integer simCardID) {
        String sql = "Delete from sim_card where id = :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", simCardID);

        int update = jdbcTemplate.update(sql, parameterSource);
        return update > 0;


    }

    public boolean add(SimCardDTO simCardDTO) {
        int update = 0;
        try {
            String sql = "insert into sim_card (number) values (:i_number)";
            MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_number", Long.valueOf(simCardDTO.getNumber()));
            update = jdbcTemplate.update(sql, parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return update > 0;
    }

    public List<ActiveUser> getAllActiveUser() {
        String sql = "select * from activeusers order by fullname";
        List<ActiveUser> users = jdbcTemplate.query(sql, new ActiveUserMapper());
        return users;
    }

    public void blockUser(Integer id) {
        String sql = "Update activeusers set blocked = true where id = :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", id);
        jdbcTemplate.update(sql,parameterSource);

    }

    public void openBlock(Integer id) {
        String sql = "Update activeusers set blocked = false where id = :i_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_id", id);
        jdbcTemplate.update(sql,parameterSource);

    }
}
