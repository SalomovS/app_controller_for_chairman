package uz.pdp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import uz.pdp.model.User;
import uz.pdp.payload.UserMapper;

import java.util.Optional;


@Repository
public class UserDAO {


    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public UserDAO(NamedParameterJdbcTemplate jdbcTemplate,
                   @Lazy PasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
    }


    public Optional<UserDetails> findByUsername(String username) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("i_username", username);
        User user = null;
        try {
            user = jdbcTemplate.queryForObject(
                    """
                            SELECT u.id          AS u_id,
                                   u.username        AS u_username,
                                   u.password    AS u_password,
                                   u.enabled     AS u_enabled,
                                   r.id          AS r_id,
                                   r.name        AS r_name,
                                   r.description AS r_description 
                            FROM users u
                                     JOIN user_role r on r.id = u.role_id
                            WHERE username = :i_username
                            """,
                    parameterSource,
                    new UserMapper()
            );

        } catch (DataAccessException e) {
            e.printStackTrace();
            return Optional.empty();
        }
        return Optional.ofNullable(user);
    }
}
